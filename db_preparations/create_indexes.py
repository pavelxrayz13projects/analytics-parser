from pymongo import MongoClient, DESCENDING, TEXT


client = MongoClient('10.0.9.118', 27017)
db = client.analytics

db.analytic_logs.create_index([('logdate', DESCENDING)])
db.analytic_logs.create_index([('sig_text', TEXT)])
