"""Сервер разбора событий IDS"""
import socket
import logging

import helpers
import parser
import settings


logger = logging.getLogger(__name__)


def start_server():
    """
    Запуск сервера
    """
    sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    sock.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
    sock.bind((settings.UDP_IP, settings.UDP_PORT))

    if 'analytic_logs' not in settings.db.collection_names():
        helpers.create_ids_collection()

    analytic_logs = helpers.BulkMongo()

    while True:
        data, _ = sock.recvfrom(1024)  # buffer size is 1024 bytes
        try:
            document = parser.parse_data(data)
        except Exception:
            logger.exception('parser error on message: {}'.format(data))
            document = None
        if document:
            analytic_logs.save(document)

if __name__ == '__main__':
    logging.basicConfig()
    start_server()
