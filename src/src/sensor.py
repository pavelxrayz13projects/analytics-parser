"""Сенсоры IDS"""
import logging
from netaddr import IPNetwork

from cachetools.func import ttl_cache

from settings import db, TTL


logger = logging.getLogger(__name__)


class Sensor():
    """
    Сенсор
    """
    def __init__(self, sensor_id, address, homenet):
        self.sensor_id = sensor_id
        self.address = address
        self.homenet = [IPNetwork(network) for network in homenet]

    def in_homenet(self, address):
        """
        Находится ли адрес в защищаемой сети
        """
        for network in self.homenet:
            if address in network:
                return True
        return False


@ttl_cache(maxsize=10000, ttl=TTL)
def get_sensors():
    """
    Получить список сенсоров
    """
    sensors = {}
    for organisation in db.organisations.find():
        for branch in organisation['branches']:
            for segment in branch['segments']:
                for sensor in segment['sensors']:
                    if sensor['type'] != 'IDS':
                        pass
                    new_sensor = Sensor(sensor['external_id'],
                                        sensor['address'],
                                        sensor['homenet'])
                    sensors[new_sensor.sensor_id] = new_sensor
    return sensors
