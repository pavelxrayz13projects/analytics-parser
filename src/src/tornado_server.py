"""Сервер для приема HTTP"""
import logging

import tornado.httpserver
import tornado.ioloop
import tornado.web
import tornado.escape

import helpers
import settings
from parser import data_to_event


logger = logging.getLogger(__name__)


class MainHandler(tornado.web.RequestHandler):
    def post(self):
        """Нам важен только факт приёма"""
        self.set_status(200)

    def on_finish(self):
        """Обработка запроса идёт здесь"""
        body = self.request.body
        if not body:
            return

        try:
            data = tornado.escape.json_decode(self.request.body)
        except Exception:
            logger.exception('parser error on message: {}'.format(body))
            data = None

        if not data:
            return

        event = data_to_event(data)
        settings.db.analytic_logs.insert_one(event)


def make_app():
    if 'analytic_logs' not in settings.db.collection_names():
        helpers.create_ids_collection()

    return tornado.web.Application([
        (r"/", MainHandler),
    ])


if __name__ == "__main__":
    app = make_app()
    server = tornado.httpserver.HTTPServer(app)
    server.bind(8080)
    server.start(6)
    tornado.ioloop.IOLoop.current().start()
