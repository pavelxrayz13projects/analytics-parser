"""Параметры системы и базы данных"""
import os

from pymongo import MongoClient, IndexModel, DESCENDING, TEXT


UDP_IP = ''
UDP_PORT = int(os.getenv('UDP_PORT', '5000'))

HTTP_PORT = int(os.getenv('HTTP_PORT', '8080'))

client = MongoClient(os.getenv('DB_HOST', '127.0.0.1'), 27017, w=0)
db = client.analytics

COLLECTION_LIMIT = int(os.getenv('COLLECTION_LIMIT', '675000000000'))
logdate_index = IndexModel([('logdate', DESCENDING)])
msg_index = IndexModel([('sig_text', TEXT)])

TTL = 30  # Время жизни кэша
