"Вспомогательные функции"
import logging
from datetime import datetime, timedelta

import pytz

import settings


logger = logging.getLogger(__name__)


TZ_EET = pytz.timezone('Europe/Kaliningrad')
TZ_MSK = pytz.timezone('Europe/Moscow')
TZ_SAMT = pytz.timezone('Europe/Samara')
TZ_YEKT = pytz.timezone('Asia/Yekaterinburg')
TZ_OMST = pytz.timezone('Asia/Omsk')
TZ_KRAT = pytz.timezone('Asia/Krasnoyarsk')
TZ_IRKT = pytz.timezone('Asia/Irkutsk')
TZ_YAKT = pytz.timezone('Asia/Yakutsk')
TZ_VLAT = pytz.timezone('Asia/Vladivostok')
TZ_MAGT = pytz.timezone('Asia/Magadan')
TZ_PETT = pytz.timezone('Asia/Kamchatka')

TZONES = {
    'EET': TZ_EET,
    'MSK': TZ_MSK,
    '+03': TZ_MSK,
    '+04': TZ_SAMT,
    'SAMT': TZ_SAMT,
    'YEKT': TZ_YEKT,
    'OMST': TZ_OMST,
    'NOVT': TZ_OMST,
    '+07': TZ_KRAT,
    'KRAT': TZ_KRAT,
    'IRKT': TZ_IRKT,
    'YAKT': TZ_YAKT,
    'VLAT': TZ_VLAT,
    'MAGT': TZ_MAGT,
    'SAKT': TZ_MAGT,
    'SRET': TZ_MAGT,
    'PETT': TZ_PETT,
    'ANAT': TZ_PETT,
}


def create_ids_collection():
    """
    Создать коллекцию для логов IDS
    """
    settings.db.create_collection(name='analytic_logs',
                                  capped=True,
                                  size=settings.COLLECTION_LIMIT)
    settings.db.analytic_logs.create_indexes([settings.logdate_index,
                                         settings.msg_index])

def parse_logdate(rtime):
    """
    Разбор поля rt
    """
    # Сначала пробуем int
    try:
        timestamp = int(rtime)/1000
        return datetime.fromtimestamp(timestamp, pytz.utc)
    except (TypeError, ValueError):
        pass

    logdate_str, tz_name = rtime.rsplit(' ', maxsplit=1)
    tzone = TZONES.get(tz_name, pytz.utc)

    try:
        logdate = datetime.strptime(logdate_str, '%b %d %Y %H:%M:%S.%f')
    except:
        logdate = datetime.strptime(logdate_str, '%b %d %Y %H:%M:%S')

    return tzone.localize(logdate)


class BulkMongo():
    """
    Класс для балк записи
    """
    def __init__(self):
        self.analytic_logs = settings.db.analytic_logs
        self.timeout = timedelta(seconds=1)
        self.data = []
        self.expire = self.timer + self.timeout
        self.db_count = self.analytic_logs.count()

    @property
    def timer(self):
        """
        Текущее время
        """
        return datetime.now()

    def save(self, doc):
        """
        Добавление в массив и отправка по таймауту
        """
        self.data.append(doc)

        if self.timer < self.expire:
            return

        self.expire = self.timer + self.timeout

        self.analytic_logs.insert_many(self.data, ordered=True)

        current_count = self.analytic_logs.count()
        parsed = len(self.data)
        saved = current_count - self.db_count
        losses = parsed - saved

        logger.info('Inserting {} docs'.format(parsed))
        if losses:
            logger.error('Losses: {} packages from {}'.format(losses, parsed))

        self.data = []
        self.db_count = current_count
