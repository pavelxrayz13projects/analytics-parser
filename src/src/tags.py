"""Модуль работы с тэгами"""
from cachetools.func import ttl_cache

from settings import db, TTL


@ttl_cache(maxsize=10000, ttl=TTL)
def get_tags():
    """
    Получить словарь тэгов
    """
    tags = db.tags.find_one({'_id': 'tags'})
    return tags['tags']
