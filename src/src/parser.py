"""Парсер логов IDS"""
import logging
from datetime import datetime
from netaddr import IPAddress

from helpers import parse_logdate
from sensor import get_sensors
from tags import get_tags


logger = logging.getLogger(__name__)

FIELDS = {
    'cn1': ('event_id', None),
    'cnt': ('cnt', int),
    'deviceExternalId': ('sensor_id', None),
    'dmac': ('dmac', None),
    'dpt': ('dpt', int),
    'dst': ('dst', None),
    'proto': ('proto', None),
    'smac': ('smac', None),
    'spt': ('spt', int),
    'src': ('src', None),
    'rt': ('logdate', parse_logdate)
}


def parse_data(data):
    """Разбор сырых данных"""
    event = {
        'type': 'ids',
    }

    _, gid_sid_rev, event['sig_text'], priority, extension = data.rsplit('|', maxsplit=4)

    event['priority'] = int(priority)
    rule = gid_sid_rev.rsplit(':', maxsplit=1)[0]
    event['rule'] = rule

    # Пропустим события аудита
    if rule == '0:0':
        return

    first_item = True
    for item in extension.split('='):
        if first_item:
            first_item = False
            current_key = item
            continue

        try:
            value, next_key = item.rsplit(' ', maxsplit=1)
        except ValueError:
            value = item

        try:
            field_name, field_func = FIELDS[current_key]
        except KeyError:
            current_key = next_key
            continue

        if field_func:
            event[field_name] = field_func(value)
        else:
            event[field_name] = value

        current_key = next_key


    # Получим информацию по сенсору
    sensor_id = event['sensor_id']
    sensors = get_sensors()
    sensor = sensors.get(sensor_id)
    if not sensor:
        logger.error('Event from unknown sensor id:{}'.format(sensor_id))
        return
    event['sensor_ip'] = sensor.address

    # Бывает в него попадает \n
    event['src'] = event['src'].strip()

    # Tags and group
    rules_tags = get_tags()
    try:
        event.update(rules_tags[rule])
    except KeyError:
        event.update({'group': 'other'})

    # Homenet
    src_in_homenet = sensor.in_homenet(IPAddress(event['src']))

    dst_in_homenet = sensor.in_homenet(IPAddress(event['dst']))

    if src_in_homenet and dst_in_homenet:
        event['homenet_address'] = [event['src'], event['dst']]
        event['direction'] = 'both'

    elif src_in_homenet:
        event['homenet_address'] = [event['src']]
        event['direction'] = 'src'

    elif dst_in_homenet:
        event['homenet_address'] = [event['dst']]
        event['direction'] = 'dst'

    else:
        logging.error('None of ip in homenet: {}; {}; sensor: {}'.format(
            event['src'],
            event['dst'],
            event['sensor_ip'],
        ))
    return event


def data_to_event(data):
    """Дополнение данными"""
    sensor_id = data['sensor_id']
    sensors = get_sensors()
    sensor = sensors.get(sensor_id)
    if not sensor:
        logger.error('Event from unknown sensor id:{}'.format(sensor_id))
        return

    data['type'] = 'ids'
    data['sensor_ip'] = sensor.address
    data['rule'] = ':'.join((data['gid'], data['sid']))

    # Remove fields
    del data['gid']
    del data['sid']
    del data['rev']
    del data['ids_version']

    # Convert to type
    data['priority'] = int(data['priority'])
    data['dpt'] = int(data['dpt'])
    data['spt'] = int(data['spt'])

    # Date
    data['logdate'] = datetime.strptime(data['logdate'], '%Y-%m-%dT%H:%M:%S.%fZ')

    # Tags and group
    rules_tags = get_tags()
    try:
        data.update(rules_tags[data['rule']])
    except KeyError:
        data.update({'group': 'other'})

    # Homenet
    src_in_homenet = sensor.in_homenet(IPAddress(data['src']))

    dst_in_homenet = sensor.in_homenet(IPAddress(data['dst']))

    if src_in_homenet and dst_in_homenet:
        data['homenet_address'] = [data['src'], data['dst']]
        data['direction'] = 'both'

    elif src_in_homenet:
        data['homenet_address'] = [data['src']]
        data['direction'] = 'src'

    elif dst_in_homenet:
        data['homenet_address'] = [data['dst']]
        data['direction'] = 'dst'

    else:
        logging.error('None of ip in homenet: {}; {}; sensor: {}'.format(
            data['src'],
            data['dst'],
            data['sensor_ip'],
        ))

    return data
