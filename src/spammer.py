import random
import socket
import string
from time import sleep

MESSAGES = 10
UDP_IP = "10.0.14.176"
UDP_PORT = 5000
#MESSAGE = "Nov  26 13:10:00 vnetids emerg CEF:0|CISCO|IDS|2.4.1-289114|1:{}:1|ek, landing, redirector, redirect, exploit, flash|3|cat=1 cn1=1067105 cn1Label=EventID cnt=1 cs1=unknown cs1Label=IDSClass cs2=preprocessor cs2Label=IDSGroup cs3= cs3Label=CVEID cs4= cs4Label=ExternalRef cs5= cs5Label=IDSTags deviceExternalId=9 deviceFacility=HTTP-server anomaly dmac=00:15:5d:04:60:58 dpt=34710 dst=91.244.183.9 proto=TCP rt=012 16 2015 12:30:00 smac=00:00:0c:07:ac:01 spt=80 src={}"
MESSAGE = "Mar  4 18:25:20 vnetids emerg CEF:0|CISCO|IDS|2.4.1-315396|1:2012600:2|ET POLICY Suspicious inbound to MSSQL port 1433|2|cat=1 cn1=106868 cn1Label=EventID cnt=1 cs1=bad-unknown cs1Label=IDSClass cs2=emerging-policy cs2Label=IDSGroup cs3= cs3Label=CVEID cs4=url,doc.emergingthreats.net/2010935 cs4Label=ExternalRef cs5= cs5Label=IDSTags deviceExternalId=1249902842 deviceFacility=Signature dmac=4c:02:89:0c:4c:c7 dpt=1433 dst=89.175.26.98 proto=TCP rt=Mar 04 2016 18:25:18.922 MSK smac=00:1c:b0:ca:57:40 spt=6000 src=61.160.236.22"

sock = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)

sock.sendto(MESSAGE.encode('utf-8'), (UDP_IP, UDP_PORT))

sleep(5)

i = 0
while i < MESSAGES:
    msg = MESSAGE

    sock.sendto(msg.encode('utf-8'), (UDP_IP, UDP_PORT))
    i += 1
